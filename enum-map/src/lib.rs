//! An enum mapping type.
//!
//! It is implemented using an array type, so using it is as fast as using Rust
//! arrays.
//!
//! # Examples
//!
//! ```
//! use enum_map::{enum_map, Enum, EnumMap};
//!
//! #[derive(Debug, Enum)]
//! enum Example {
//!     A,
//!     B,
//!     C,
//! }
//!
//! fn main() {
//!     let mut map = enum_map! {
//!         Example::A => 1,
//!         Example::B => 2,
//!         Example::C => 3,
//!     };
//!     map[Example::C] = 4;
//!
//!     assert_eq!(map[Example::A], 1);
//!
//!     for (key, &value) in &map {
//!         println!("{:?} has {} as value.", key, value);
//!     }
//! }
//! ```

#![no_std]
#![deny(missing_docs)]

mod enum_map_impls;
mod internal;
mod iter;
mod serde;

pub use enum_map_derive::Enum;
pub use internal::Enum;
pub use iter::{IntoIter, Iter, IterMut, Values, ValuesMut};

/// Enum map constructor.
///
/// This macro allows to create a new enum map in a type safe way. It takes
/// a list of `,` separated pairs separated by `=>`. Left side is `|`
/// separated list of enum keys, or `_` to match all unmatched enum keys,
/// while right side is a value.
///
/// # Examples
///
/// ```
/// # extern crate enum_map;
/// use enum_map::{enum_map, Enum};
///
/// #[derive(Enum)]
/// enum Example {
///     A,
///     B,
///     C,
///     D,
/// }
///
/// fn main() {
///     let enum_map = enum_map! {
///         Example::A | Example::B => 1,
///         Example::C => 2,
///         _ => 3,
///     };
///     assert_eq!(enum_map[Example::A], 1);
///     assert_eq!(enum_map[Example::B], 1);
///     assert_eq!(enum_map[Example::C], 2);
///     assert_eq!(enum_map[Example::D], 3);
/// }
/// ```
#[macro_export]
macro_rules! enum_map {
    {$($t:tt)*} => {
        $crate::__from_fn(|k| match k { $($t)* })
    };
}

/// Owned enum map constructor.
///
/// This macro allows to create a new enum map in a type safe way. It takes
/// a list of `,` separated pairs separated by `=>`. Left side is a enum keys
/// (i.e. a enum variant literal), while right side is a value (i.e. an
/// expression).
///
/// The difference to the [`enum_map`](macro.enum_map.html) macro is, that
/// there may be no multi variant branches nor a 'catch all' branch, instead
/// every variant must be individually specified. The benefit of this design
/// now is, that values may be captured by value (i.e. moved into the map).
///
/// # Examples
///
/// For simple use cases, this macro works pretty like [`enum_map`](macro.enum_map.html):
///
/// ```
/// # extern crate enum_map;
/// use enum_map::{enum_map_move, Enum, EnumMap};
///
/// #[derive(Enum, PartialEq, Eq, Hash)]
/// enum Example {
///     A,
///     B,
///     C,
/// }
///
/// fn main() {
///     let enum_map = enum_map_move! {
///         Example::A => 1,
///         Example::B => 1,
///         Example::C => 2,
///     };
///     assert_eq!(enum_map[Example::A], 1);
///     assert_eq!(enum_map[Example::B], 1);
///     assert_eq!(enum_map[Example::C], 2);
/// }
/// ```
///
/// But now, also allows to specified owned data such as:
///
/// ```
/// # extern crate enum_map;
/// use enum_map::{enum_map_move, Enum, EnumMap};
///
/// #[derive(Enum, PartialEq, Eq, Hash)]
/// enum Example {
///     A,
///     B,
///     C,
/// }
///
/// fn main() {
///     let a = "a".to_string();
///     let b = "B".to_string();
///     let mut c = String::new();
///
///     let mut enum_map = enum_map_move! {
///         Example::A => a,
///         Example::B => b,
///         Example::C => {
///             c.push('c');
///             c
///         },
///     };
///     assert_eq!(enum_map[Example::A], "a");
///     assert_eq!(enum_map[Example::B], "B");
///     assert_eq!(enum_map[Example::C], "c");
///
///     enum_map[Example::B] = "b".to_string();
///     assert_eq!(enum_map[Example::B], "b");
/// }
/// ```
///
/// Also, a compile time error is generated if any variant is not covered.
///
/// ```compile_fail
/// # extern crate enum_map;
/// use enum_map::{enum_map_move, Enum, EnumMap};
///
/// #[derive(Enum, PartialEq, Eq, Hash)]
/// enum Example {
///     A,
///     B,
/// }
///
/// fn main() {
///     let enum_map: EnumMap<Example, u32> = enum_map_move! {
///         Example::A => 1,
///         // Example::B not covered => Compile time error
///     };
/// }
/// ```
///
/// # Current Issues
///
/// * When using `#[non_exhaustive]` enums defined by another crate,
///   this macro will not compile, asking to add the wildcard branch, which is
///   in turn not allowed by this macro.
/// * Only enums which are `Eq` & `Hash` may be used with this macro.
/// * May only be used in crates using the standard lib.
///
///
#[macro_export]
macro_rules! enum_map_move {
	{ $( $variant:path => $expr:expr ),+ $(,)? } => {{
		let mut values = std::collections::HashMap::new();

		// Hack, just to make omitting a variant a compile-time error.
		if false {
			match unreachable!() {
				$( $variant => () ),*
			}
		}

		$( values.insert( $variant , Some( $expr ) ); )*

		$crate::__from_fn(|k| {
			values.get_mut(&k)
				// Must not panic, since above hack guarantees that all variants are covered.
				.unwrap()
				.take()
				// Does not panic, if the Enum impl visits every variant at most once.
				.unwrap()
		})
	}};
}

/// An enum mapping.
///
/// This internally uses an array which stores a value for each possible
/// enum value. To work, it requires implementation of internal (private,
/// although public due to macro limitations) trait which allows extracting
/// information about an enum, which can be automatically generated using
/// `#[derive(Enum)]` macro.
///
/// Additionally, `bool` and `u8` automatically derives from `Enum`. While
/// `u8` is not technically an enum, it's convenient to consider it like one.
/// In particular, [reverse-complement in benchmark game] could be using `u8`
/// as an enum.
///
/// # Examples
///
/// ```
/// # extern crate enum_map;
/// use enum_map::{enum_map, Enum, EnumMap};
///
/// #[derive(Enum)]
/// enum Example {
///     A,
///     B,
///     C,
/// }
///
/// fn main() {
///     let mut map = EnumMap::new();
///     // new initializes map with default values
///     assert_eq!(map[Example::A], 0);
///     map[Example::A] = 3;
///     assert_eq!(map[Example::A], 3);
/// }
/// ```
///
/// [reverse-complement in benchmark game]:
///     http://benchmarksgame.alioth.debian.org/u64q/program.php?test=revcomp&lang=rust&id=2
pub struct EnumMap<K: Enum<V>, V> {
    array: K::Array,
}

impl<K: Enum<V>, V: Default> EnumMap<K, V> {
    /// Creates an enum map with default values.
    ///
    /// # Examples
    ///
    /// ```
    /// # extern crate enum_map;
    /// use enum_map::{Enum, EnumMap};
    ///
    /// #[derive(Enum)]
    /// enum Example {
    ///     A,
    /// }
    ///
    /// fn main() {
    ///     let enum_map = EnumMap::<_, i32>::new();
    ///     assert_eq!(enum_map[Example::A], 0);
    /// }
    /// ```
    #[inline]
    pub fn new() -> Self {
        EnumMap::default()
    }

    /// Clear enum map with default values.
    ///
    /// # Examples
    ///
    /// ```
    /// # extern crate enum_map;
    /// use enum_map::{Enum, EnumMap};
    ///
    /// #[derive(Enum)]
    /// enum Example {
    ///     A,
    ///     B,
    /// }
    ///
    /// fn main() {
    ///     let mut enum_map = EnumMap::<_, String>::new();
    ///     enum_map[Example::B] = "foo".into();
    ///     enum_map.clear();
    ///     assert_eq!(enum_map[Example::A], "");
    ///     assert_eq!(enum_map[Example::B], "");
    /// }
    /// ```
    #[inline]
    pub fn clear(&mut self) {
        for v in self.as_mut_slice() {
            *v = V::default();
        }
    }
}

impl<K: Enum<V>, V> EnumMap<K, V> {
    /// Returns an iterator over enum map.
    #[inline]
    pub fn iter(&self) -> Iter<K, V> {
        self.into_iter()
    }

    /// Returns a mutable iterator over enum map.
    #[inline]
    pub fn iter_mut(&mut self) -> IterMut<K, V> {
        self.into_iter()
    }

    /// Returns number of elements in enum map.
    #[inline]
    pub fn len(&self) -> usize {
        self.as_slice().len()
    }

    /// Returns whether the enum variant set is empty.
    ///
    /// This isn't particularly useful, as there is no real reason to use
    /// enum map for enums without variants. However, it is provided for
    /// consistency with data structures providing len method (and I will
    /// admit, to avoid clippy warnings).
    ///
    /// # Examples
    ///
    /// ```
    /// # extern crate enum_map;
    /// use enum_map::{Enum, EnumMap};
    ///
    /// #[derive(Enum)]
    /// enum Void {}
    ///
    /// #[derive(Enum)]
    /// enum SingleVariant {
    ///     Variant,
    /// }
    ///
    /// fn main() {
    ///     assert!(EnumMap::<Void, ()>::new().is_empty());
    ///     assert!(!EnumMap::<SingleVariant, ()>::new().is_empty());
    /// }
    /// ```
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.as_slice().is_empty()
    }

    /// Swaps two indexes.
    ///
    /// # Examples
    ///
    /// ```
    /// # extern crate enum_map;
    /// use enum_map::enum_map;
    ///
    /// fn main() {
    ///     let mut map = enum_map! { false => 0, true => 1 };
    ///     map.swap(false, true);
    ///     assert_eq!(map[false], 1);
    ///     assert_eq!(map[true], 0);
    /// }
    /// ```
    #[inline]
    pub fn swap(&mut self, a: K, b: K) {
        self.as_mut_slice().swap(a.to_usize(), b.to_usize())
    }

    /// Converts an enum map to a slice representing values.
    #[inline]
    pub fn as_slice(&self) -> &[V] {
        K::slice(&self.array)
    }

    /// Converts a mutable enum map to a mutable slice representing values.
    #[inline]
    pub fn as_mut_slice(&mut self) -> &mut [V] {
        K::slice_mut(&mut self.array)
    }

    /// Returns a raw pointer to the enum map's slice.
    ///
    /// The caller must ensure that the slice outlives the pointer this
    /// function returns, or else it will end up pointing to garbage.
    ///
    /// # Examples
    ///
    /// ```
    /// # extern crate enum_map;
    /// use enum_map::{enum_map, EnumMap};
    ///
    /// fn main() {
    ///     let map = enum_map! { 5 => 42, _ => 0 };
    ///     assert_eq!(unsafe { *map.as_ptr().offset(5) }, 42);
    /// }
    /// ```
    #[inline]
    pub fn as_ptr(&self) -> *const V {
        self.as_slice().as_ptr()
    }

    /// Returns an unsafe mutable pointer to the enum map's slice.
    ///
    /// The caller must ensure that the slice outlives the pointer this
    /// function returns, or else it will end up pointing to garbage.
    ///
    /// # Examples
    ///
    /// ```
    /// # extern crate enum_map;
    /// use enum_map::{enum_map, EnumMap};
    ///
    /// fn main() {
    ///     let mut map = enum_map! { _ => 0 };
    ///     unsafe {
    ///         *map.as_mut_ptr().offset(11) = 23
    ///     };
    ///     assert_eq!(map[11], 23);
    /// }
    /// ```
    #[inline]
    pub fn as_mut_ptr(&mut self) -> *mut V {
        self.as_mut_slice().as_mut_ptr()
    }
}

impl<F: FnMut(K) -> V, K: Enum<V>, V> From<F> for EnumMap<K, V> {
    #[inline]
    fn from(f: F) -> Self {
        EnumMap {
            array: K::from_function(f),
        }
    }
}

#[doc(hidden)]
pub fn __from_fn<K, V>(f: impl FnMut(K) -> V) -> EnumMap<K, V>
where
    K: Enum<V>,
{
    f.into()
}
